﻿using System.Diagnostics;
using System.Text.Json;

namespace SerializationCSV
{
    internal static class TestSpeed
    {
        private static readonly IConverter converter = new Converter();

        public static string Timer<T>(int count,   Func<T> func, bool UseOutPut = false)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (int i = 0; i < count; i++)
            {
                var t = func();
                if (UseOutPut) Console.WriteLine(t);
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds);
            return $"Количество замеров {count}. Время на сериализацию = {elapsedTime}";
        }

        public static void Run()
        {
            var _100Serialization = Timer(100,   () => converter.Serialization(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), false);
            var _100000Serialization = Timer(100000,   () => converter.Serialization(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), false);
            var _100OutSerialization = Timer(100, () => converter.Serialization(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), true);
            var _100000OutSerialization = Timer(100000, () => converter.Serialization(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), true);

            var _100StandartSerialization = Timer(100, () => JsonSerializer.Serialize(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), false);
            var _100000StandartSerialization = Timer(100000, () => JsonSerializer.Serialize(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), false);
            var _100OutStandartSerialization = Timer(100, () => JsonSerializer.Serialize(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), true);
            var _100000OutStandartSerialization = Timer(100000, () => JsonSerializer.Serialize(new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }), true);


            var _100Derialization = Timer(100, () => converter.Derialization<F>($"i1;i2;i3;i4;i5{Environment.NewLine}1; 2; 3; 4; 5"), false);
            var _100000Derialization = Timer(100000, () => converter.Derialization<F>($"i1;i2;i3;i4;i5{Environment.NewLine}1; 2; 3; 4; 5"), false);

          
            Console.WriteLine();
            Console.WriteLine("SerializationCSV");
            Console.WriteLine($"Без вывода в консоль:{Environment.NewLine}{_100Serialization}{Environment.NewLine}{_100000Serialization}{Environment.NewLine}" +
                $"Вывод в консоль:{Environment.NewLine}{_100OutSerialization}{Environment.NewLine}{_100000OutSerialization}");
            
            Console.WriteLine();
            Console.WriteLine("Стандартный механизм System.Text.Json");
            Console.WriteLine($"Без вывода в консоль:{Environment.NewLine}{_100StandartSerialization}{Environment.NewLine}{_100000StandartSerialization}{Environment.NewLine}" +
            $"Вывод в консоль:{Environment.NewLine}{_100OutStandartSerialization}{Environment.NewLine}{_100000OutStandartSerialization}");

            Console.WriteLine();
            Console.WriteLine("DerializationCSV");
            Console.WriteLine($"Без вывода в консоль:{Environment.NewLine}{_100Derialization}{Environment.NewLine}{_100000Derialization}{Environment.NewLine}");

            
        }

    }

    internal class F
    {
        public int i1 { get; set; }
        public int i2 { get; set; }
        public int i3 { get; set; }
        public int i4 { get; set; }
        public int i5 { get; set; }
    }
}

﻿namespace SerializationCSV
{
    public interface IConverter
    {
        public string Serialization<T>(T obj);
        public T Derialization<T>(string obj, string separator = ";");
    }
}
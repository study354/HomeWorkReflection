﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SerializationCSV
{
    public class Converter: IConverter
    {

        public string Serialization<T>(T obj)
        {
            if (obj == null) throw new ArgumentNullException("Geting null object)");
            var sb = new StringBuilder();
            
            var fields = typeof(T).GetProperties();
            sb = SerializationNameProperties(fields, sb);
            sb.AppendLine();
            sb = SerializationOneObject(obj, fields, sb);
            return sb.ToString();
        }

        public T Derialization<T>(string obj, string separator=";")
        {
            if (obj == null) throw new ArgumentNullException("Geting null object)");

            T temp = (T)CreateObject<T>(typeof(T));
            var namesProperties = typeof(T).GetProperties();

            var s = Regex.Split(obj, Environment.NewLine);

            var fieldsName = Regex.Split(s[0], separator);
            var fields = Regex.Split(s[1], separator);
            if (fieldsName.Length != fields.Length) throw new InvalidDataException("Error in formating CSV. fieldsName.Length != fields.Length");
            for(int i=0; i<fieldsName.Length; i++)
            {
                var propety = namesProperties.FirstOrDefault(x => x.Name == fieldsName[i]);
                if (propety == null) throw new InvalidDataException($"Not found property witn name {fieldsName[i]}");
                propety.SetValue(temp, Convert.ChangeType(fields[i], propety.PropertyType));
            }


            return temp;
        }

        private object CreateObject<T>(Type type)
        {
            var t = type.GetConstructors().First();
            return t.Invoke(null);
        }

        private StringBuilder SerializationOneObject<T>(T obj, PropertyInfo[] properties,  StringBuilder sb)
        {
            return sb.Append(string.Join(";", properties.Select(x => x.GetValue(obj))));
        }

        private StringBuilder SerializationNameProperties(PropertyInfo[] properties, StringBuilder sb)
        {
            return sb.Append(string.Join(";", properties.Select(x => x.Name)));
        }
    }
}
